﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmProje_1.Controllers
{
    public class HomeController : Controller
    {
        FilmProjeEntities db = new FilmProjeEntities();
        public ActionResult Index()
        {
            var liste = db.Film.Where(x => x.filmID == 7);
            return View(liste);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [ChildActionOnly]
        public ActionResult _Guncel(string name)
        {
            
            var liste = db.Film.Where(x=>x.filmID<7);
            return View(liste);
        }
        public ActionResult Data()
        {
            var liste = db.Film.OrderBy(x => x.filmID);
            return View(liste);
        }
      
      
    }
}