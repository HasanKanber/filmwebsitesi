﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmProje_1.Areas.Admin.Controllers
{
    public class PanelController : Controller
    {
        FilmProjeEntities db = new FilmProjeEntities();

        // GET: Admin/Panel
        public ActionResult Index()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }
            return View();
        }

        public ActionResult AdminName()
        {
           
            int a = int.Parse(Session["user"].ToString());
            var k = db.tblAdmin.Find(a);
            ViewBag.ad=k.AdminName;

            return View();
        }
       
        public ActionResult Giris(string AdminName,string AdminPass)
        {
            var varmi = db.tblAdmin.Where(x => x.AdminName == AdminName && x.AdminPass == AdminPass).FirstOrDefault();
            if (varmi != null)
            {
                Session["user"]=varmi.AdminID;
                return RedirectToAction("Index", "Panel");
            }
            else
            {
                ViewBag.GirisHata = "Giriş Yapılmadı";
                RedirectToAction("Giris", "Panel");
            }

           
            return View();
        }
        public ActionResult Cikis()
        {
            Session["user"] = null;
            return RedirectToAction("Giris", "Panel");
           
        }
       
        [HttpGet]
        public ActionResult BizeUlasSil(int? id)
       {
            try
            {
                var k = db.Cantact.Find(id);
                return View(k);
            }
            catch (Exception)
            {
                ViewBag.Hata = "Kategori eklenirken Hatalar Oluştu.";
                return View();
            }
            

        }

        [HttpPost]
        public ActionResult BizeUlasSil(int id)
        {
            try
            {
                var k = db.Cantact.Find(id);
                db.Cantact.Remove(k);
                db.SaveChanges();
                return RedirectToAction("index","panel");
            }
            catch (Exception)
            {
                ViewBag.Hata = "Mesaj silinirken Hatalar Oluştu.";
                return View();
            }
        }
    }
}