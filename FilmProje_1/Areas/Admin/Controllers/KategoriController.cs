﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FilmProje_1.Areas.Admin.Controllers
{
    public class KategoriController : Controller
    {
        FilmProjeEntities db = new FilmProjeEntities();
        public ActionResult Index()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }

            try
            {
                return View(db.Category.ToList());
            }
            catch (Exception)
            {
                ViewBag.Hata = "Kategori eklenirken Hatalar Oluştu.";
                return View();
            }
        }
        public ActionResult Ekle()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }

            return View();
        }
        [HttpPost]
        public ActionResult Ekle(Category k)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    int say = db.Category.Where(x => x.name.ToLower() == k.name.ToLower()).Count();
                    if (say == 0)
                    {
                        k.name = k.name.ToUpper();
                        db.Category.Add(k);
                        db.SaveChanges();
                        ViewBag.Mesaj = "Kategori Eklendi.";
                    }
                    else
                    {
                        ModelState.AddModelError("", "böyle bir kategori zaten var.");
                    }
                }
                catch (Exception)
                {
                    ViewBag.Hata = "Kategori eklenirken Hatalar Oluştu.";
                }
            }
            return View();
        }
        [HttpGet]
        public ActionResult Sil(int? id)
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }

            if (id == null)
                return RedirectToAction("Index");
            try
            {
                var k = db.Category.Find(id);
                return View(k);
            }
            catch (Exception)
            {
                ViewBag.Hata = "Kategori eklenirken Hatalar Oluştu.";
                return View();
            }

        }
        [HttpPost]
        public ActionResult Sil(int id)
        {
            try
            {
                var k = db.Category.Find(id);
                db.Category.Remove(k);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                ViewBag.Hata = "Kategori silinirken Hatalar Oluştu.";
                return View();
            }
        }
        public ActionResult Guncelle(int? id)
        {
            if (id == null)
                return RedirectToAction("index");
            try
            {
                var k = db.Category.Find(id);
                return View(k);
            }
            catch (Exception)
            {

                ViewBag.Hata = "Kategori eklenirken Hatalar Oluştu.";
                return View();
            }
        }
        [HttpPost]
        public ActionResult Guncelle(Category k,int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var m = db.Category.Find(id);
                    m.name = k.name.ToUpper();
                    db.SaveChanges();
                    return RedirectToAction("index");
                }
            }
            catch (Exception)
            {
                ViewBag.Hata = "Kategori Guncellerken Hatalar Oluştu.";
            }
            return View();
        }
    }
}