﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FilmProje_1;
namespace FilmProje_1.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin/Admin
        FilmProjeEntities db = new FilmProjeEntities();
        public ActionResult Index()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }

            try
            {
                return View(db.tblAdmin.ToList());
            }
            catch (Exception)
            {
                ViewBag.Hata = "Admin eklenirken Hatalar Oluştu.";
                return View();
            }

        }
        public ActionResult Ekle()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }
            return View();
           
        }
        [HttpPost]
        public ActionResult Ekle(tblAdmin k)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    int say = db.tblAdmin.Where(x => x.AdminName.ToLower() == k.AdminName.ToLower()).Count();
                    if (say == 0)
                    {
                        k.AdminName = k.AdminName.ToUpper();
                        db.tblAdmin.Add(k);
                        db.SaveChanges();

                    }
                    else
                    {
                        ModelState.AddModelError("", "böyle bir Admin zaten var.");
                    }
                }
                catch (Exception)
                {
                    ViewBag.Hata = "Admin eklenirken Hatalar Oluştu.";
                }
            }
            return View();
        }
        public ActionResult Sil(int? id)
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }

            try
            {
                var k = db.tblAdmin.Find(id);
                return View(k);
            }
            catch (Exception)
            {
                ViewBag.Hata = "Admin Silinirken Hatalar Oluştu.";
                return View();
            }

        }
        [HttpPost]
        public ActionResult Sil( int id )
        {
            try
            {
                var k = db.tblAdmin.Find(id);
                db.tblAdmin.Remove(k);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception) { 
         
                ViewBag.Hata = "Admin silinirken Hatalar Oluştu.";
                return View();
            }

        }
        public ActionResult Guncelle(int? id)
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }

            if (id == null)
                return RedirectToAction("index");
            try
            {
                var k = db.tblAdmin.Find(id);
                return View(k);
            }
            catch (Exception)
            {

                ViewBag.Hata = "Admin Güncellenirken Hatalar Oluştu.";
                return View();
            }
        }
        [HttpPost]
        public ActionResult Guncelle(tblAdmin k, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var m = db.tblAdmin.Find(id);
                    m.AdminName = k.AdminName.ToUpper();
                    m.AdminPass = k.AdminPass;
                    db.SaveChanges();
                    ViewBag.Mesaj = "Başarıyla Güncellendi";
                    return RedirectToAction("index");

                }
            }
            catch (Exception)
            {
                ViewBag.Hata = "Admin Guncellerken Hatalar Oluştu.";
            }
            return View();
        }
    }
}