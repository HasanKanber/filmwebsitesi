﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
namespace FilmProje_1.Areas.Admin.Controllers
{
    public class FilmController : Controller
    {
        FilmProjeEntities db = new FilmProjeEntities();
        public ActionResult Index()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }

            try
            {
                ViewBag.Film = db.Film.OrderBy(x => x.title).ToList();
                return View(db.Film.ToList());
            }
            catch (Exception)
            {
                ViewBag.Hata = "No film exsist!";
                return View();
            }
        }
        public ActionResult Ekle()
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }

            ViewBag.Kategori = db.Category.OrderBy(x => x.name).ToList();
            return View();
        }
        [HttpPost]
        public ActionResult Ekle(Film m)
        {
            m.miniImagePath = "/assets/img/film/";
            if (ModelState.IsValid)
            {
                try
                {
                    if (Request.Files.Count > 0)
                    {
                        var file = Request.Files[0];

                        if (file != null && file.ContentLength > 0)
                        {
                            string[] uzantilar = { ".JPG", ".JPEG", ".PNG", ".BMP", ".GIF" };
                            Random sayi = new Random();
                            int nameID = sayi.Next(1, 10000); ;

                            for (int i = 0; i < uzantilar.Count(); i++)
                            {
                                if (file.FileName.ToString().ToUpper().EndsWith(uzantilar[i]))
                                {
                                    m.miniImagePath += m.title + nameID.ToString() + uzantilar[i];
                                    db.Film.Add(m);
                                    db.SaveChanges();
                                    var path = Path.Combine(Server.MapPath("~/assets/img/film/"), m.title + nameID.ToString() + uzantilar[i]);
                                    file.SaveAs(path);
                                    ViewBag.Mesaj = "Film Başarıyla eklendi.";
                                    return RedirectToAction("index");
                                }
                            }
                        }
                    }
                    return RedirectToAction("index");
                }
                catch (Exception)
                {
                    ViewBag.Hata = "Film eklenirken Hatalar Oluştu.";
                }
            }
            return View();
        }
        [HttpGet]
        public ActionResult Sil(int? id)
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }

            if (id == null)
                return RedirectToAction("Index");
            try
            {
                var k = db.Film.Find(id);
                return View(k);
            }
            catch (Exception)
            {
                ViewBag.Hata = "Film eklenirken Hatalar Oluştu.";
                return View();
            }
        }
        [HttpPost]
        public ActionResult Sil(int id)
        {
            try
            {
                var k = db.Film.Find(id);
                db.Film.Remove(k);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                ViewBag.Hata = "Film silinirken Hatalar Oluştu.";
                return View();
            }
        }

        public ActionResult Guncelle(int? id)
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Giris", "Panel");
            }
            try
            {
                ViewBag.Kategori = db.Category.OrderBy(x => x.name).ToList();
                var k = db.Film.Find(id);

                return View(k);
            }
            catch (Exception)
            {

                return RedirectToAction("index");
            }



        }
        [HttpPost]
        public ActionResult Guncelle(Film k, int id)
        {
            try
            {
               
                var m = db.Film.Find(id);
                m.miniImagePath = "/assets/img/film/";
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];

                    if (file != null && file.ContentLength > 0)
                    {
                        string[] uzantilar = { ".JPG", ".JPEG", ".PNG", ".BMP", ".GIF" };
                        Random sayi = new Random();
                        int nameID = sayi.Next(1, 10000); ;

                        for (int i = 0; i < uzantilar.Count(); i++)
                        {
                            if (file.FileName.ToString().ToUpper().EndsWith(uzantilar[i]))
                            {
                                m.title = k.title.ToUpper();
                                m.descripton = k.descripton;

                                //m.YonetmenAdi = k.YonetmenAdi;
                                //m.tbl_KategoriID = k.tbl_KategoriID;
                                //m.resimUrl += m.FilmAdi + nameID.ToString() + uzantilar[i];

                                db.SaveChanges();
                                var path = Path.Combine(Server.MapPath("~/assets/img/film/"), m.title + nameID.ToString() + uzantilar[i]);
                                file.SaveAs(path);
                                ViewBag.Mesaj = "Film Başarıyla güncellendi.";
                                return RedirectToAction("index");
                            }
                        }
                    }
                }
                return RedirectToAction("index");
            }
            catch (Exception)
            {
                ViewBag.Hata = "Film eklenirken Hatalar Oluştu.";
            }
            return RedirectToAction("index");
        }
    }
}
