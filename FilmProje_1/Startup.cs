﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FilmProje_1.Startup))]
namespace FilmProje_1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
